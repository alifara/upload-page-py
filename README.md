# Upload Page

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

An upload page written in python and flask for exchanging files over a trusted network **WITHOUT** encryption, using http protocol.

#### How do I use it?

Just clone the repo and type `./run.sh`. 
If you don't have the dependencies installed, create a virtualenv and install them there.

---

### Screenshots

* Upload Page

![alt Upload_Page](.res/screenshots/upload_pls.png)


* After Clicking `Submit`

![alt Upload_Successful](.res/screenshots/upload_successful.png)
