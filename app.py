from flask import Flask
from uuid import uuid4

UPLOAD_FOLDER = "./Uploads/"
MAX_FILESIZE = 4 * (2 << 30)  # 4 GiB

app = Flask(__name__)
app.secret_key = str(uuid4())
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["MAX_CONTENT_LENGTH"] = MAX_FILESIZE
