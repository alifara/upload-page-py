import os

# import magic
import urllib.request
from app import app, MAX_FILESIZE
from flask import Flask, flash, request, redirect, render_template
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(["txt", "pdf", "png", "jpg", "jpeg", "gif"])
PORT = 5000
Debug = False

# TODO:
#   use pathlib.Path module insted of os.mkdir and os.path.


def allowed_file(filename):
    # TODO: replace it with regex or pathlib.Path
    # return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    return True


@app.route("/")
def upload_form():
    return render_template("upload.html", max_upload=MAX_FILESIZE // 2 ** 20)


@app.route("/", methods=["POST"])
def upload_file():
    if request.method == "POST":
        # check if the post request has the file part
        if "file" not in request.files:
            flash("No file part")
            return redirect(request.url)
        file = request.files["file"]
        if file.filename == "":
            flash("No file selected for uploading")
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # Make sure folder exists
            if not os.path.exists(upload_dir := app.config["UPLOAD_FOLDER"]):
                os.mkdir(upload_dir)
            filepath = os.path.join(upload_dir, filename)
            file.save(filepath)
            flash("File successfully uploaded")
            flash(f"File size: {(os.path.getsize(filepath) / 2**20):.4f} MiB")
            return redirect("/suc")
        else:
            # flash('Allowed file types are txt, pdf, png, jpg, jpeg, gif')
            flash(f'Allowed file types are {"".join(ALLOWED_EXTENSIONS)}')
            return redirect(request.url)


@app.route("/suc", methods=["GET"])
def display_successful_upload():
    return render_template("successful.html")


if __name__ == "__main__":
    if Debug:
        app.run(host="::1", port=PORT)
    else:
        app.run(host="::", port=PORT)
