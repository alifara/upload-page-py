#!/bin/sh
gunicorn main:app -b [::]:5000 -w 3 --threads 3 --access-logfile '-'
